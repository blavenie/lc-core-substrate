// Auto-generated via `yarn polkadot-types-from-chain`, do not edit
/* eslint-disable */

import type { ApiTypes } from '@polkadot/api/types';

declare module '@polkadot/api/types/errors' {
  export interface AugmentedErrors<ApiType> {
    balances: {
      /**
       * Beneficiary account must pre-exist
       **/
      DeadAccount: AugmentedError<ApiType>;
      /**
       * Value too low to create account due to existential deposit
       **/
      ExistentialDeposit: AugmentedError<ApiType>;
      /**
       * A vesting schedule already exists for this account
       **/
      ExistingVestingSchedule: AugmentedError<ApiType>;
      /**
       * Balance too low to send value
       **/
      InsufficientBalance: AugmentedError<ApiType>;
      /**
       * Transfer/payment would kill account
       **/
      KeepAlive: AugmentedError<ApiType>;
      /**
       * Account liquidity restrictions prevent withdrawal
       **/
      LiquidityRestrictions: AugmentedError<ApiType>;
      /**
       * Number of named reserves exceed MaxReserves
       **/
      TooManyReserves: AugmentedError<ApiType>;
      /**
       * Vesting balance too high to send value
       **/
      VestingBalance: AugmentedError<ApiType>;
    };
    grandpa: {
      /**
       * Attempt to signal GRANDPA change with one already pending.
       **/
      ChangePending: AugmentedError<ApiType>;
      /**
       * A given equivocation report is valid but already previously reported.
       **/
      DuplicateOffenceReport: AugmentedError<ApiType>;
      /**
       * An equivocation proof provided as part of an equivocation report is invalid.
       **/
      InvalidEquivocationProof: AugmentedError<ApiType>;
      /**
       * A key ownership proof provided as part of an equivocation report is invalid.
       **/
      InvalidKeyOwnershipProof: AugmentedError<ApiType>;
      /**
       * Attempt to signal GRANDPA pause when the authority set isn't live
       * (either paused or already pending pause).
       **/
      PauseFailed: AugmentedError<ApiType>;
      /**
       * Attempt to signal GRANDPA resume when the authority set isn't paused
       * (either live or already pending resume).
       **/
      ResumeFailed: AugmentedError<ApiType>;
      /**
       * Cannot signal forced change so soon after last.
       **/
      TooSoon: AugmentedError<ApiType>;
    };
    identity: {
      /**
       * Identity already confirmed
       **/
      IdtyAlreadyConfirmed: AugmentedError<ApiType>;
      /**
       * Identity already exist
       **/
      IdtyAlreadyExist: AugmentedError<ApiType>;
      /**
       * Identity already validated
       **/
      IdtyAlreadyValidated: AugmentedError<ApiType>;
      /**
       * You are not allowed to create a new identity now
       **/
      IdtyCreationNotAllowed: AugmentedError<ApiType>;
      /**
       * Identity not confirmed by owner
       **/
      IdtyNotConfirmedByOwner: AugmentedError<ApiType>;
      /**
       * Identity not found
       **/
      IdtyNotFound: AugmentedError<ApiType>;
      /**
       * Identity not validated
       **/
      IdtyNotValidated: AugmentedError<ApiType>;
      /**
       * Identity not yet renewable
       **/
      IdtyNotYetRenewable: AugmentedError<ApiType>;
      /**
       * This operation requires to be the owner of the identity
       **/
      RequireToBeOwner: AugmentedError<ApiType>;
      /**
       * Right already added
       **/
      RightAlreadyAdded: AugmentedError<ApiType>;
      /**
       * Right not exist
       **/
      RightNotExist: AugmentedError<ApiType>;
    };
    strongCert: {
      /**
       * An identity must receive certifications before it can issue them.
       **/
      IdtyMustReceiveCertsBeforeCanIssue: AugmentedError<ApiType>;
      /**
       * This identity has already issued the maximum number of certifications
       **/
      IssuedTooManyCert: AugmentedError<ApiType>;
      /**
       * This identity has already issued a certification too recently
       **/
      NotRespectCertPeriod: AugmentedError<ApiType>;
      /**
       * This certification has already been issued or renewed recently
       **/
      NotRespectRenewablePeriod: AugmentedError<ApiType>;
    };
    sudo: {
      /**
       * Sender must be the Sudo account
       **/
      RequireSudo: AugmentedError<ApiType>;
    };
    system: {
      /**
       * Failed to extract the runtime version from the new runtime.
       * 
       * Either calling `Core_version` or decoding `RuntimeVersion` failed.
       **/
      FailedToExtractRuntimeVersion: AugmentedError<ApiType>;
      /**
       * The name of specification does not match between the current runtime
       * and the new runtime.
       **/
      InvalidSpecName: AugmentedError<ApiType>;
      /**
       * Suicide called when the account has non-default composite data.
       **/
      NonDefaultComposite: AugmentedError<ApiType>;
      /**
       * There is a non-zero reference count preventing the account from being purged.
       **/
      NonZeroRefCount: AugmentedError<ApiType>;
      /**
       * The specification version is not allowed to decrease between the current runtime
       * and the new runtime.
       **/
      SpecVersionNeedsToIncrease: AugmentedError<ApiType>;
    };
  }

  export interface DecoratedErrors<ApiType extends ApiTypes> extends AugmentedErrors<ApiType> {
  }
}
