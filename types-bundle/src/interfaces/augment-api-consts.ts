// Auto-generated via `yarn polkadot-types-from-chain`, do not edit
/* eslint-disable */

import type { Vec, u16 } from '@polkadot/types';
import type { Balance, BalanceOf, BlockNumber, Moment, Permill, RuntimeDbWeight } from '@polkadot/types/interfaces/runtime';
import type { RuntimeVersion } from '@polkadot/types/interfaces/state';
import type { WeightToFeeCoefficient } from '@polkadot/types/interfaces/support';
import type { BlockLength, BlockWeights } from '@polkadot/types/interfaces/system';
import type { ApiTypes } from '@polkadot/api/types';

declare module '@polkadot/api/types/consts' {
  export interface AugmentedConsts<ApiType> {
    balances: {
      /**
       * The minimum amount required to keep an account open.
       **/
      existentialDeposit: Balance & AugmentedConst<ApiType>;
    };
    identity: {
      /**
       * Period during which the owner can confirm the new identity.
       **/
      confirmPeriod: BlockNumber & AugmentedConst<ApiType>;
      /**
       * Maximum period of inactivity, after this period, the identity is permanently deleted
       **/
      maxInactivityPeriod: BlockNumber & AugmentedConst<ApiType>;
      /**
       * Maximum period with no rights, after this period, the identity is permanently deleted
       **/
      maxNoRightPeriod: BlockNumber & AugmentedConst<ApiType>;
      /**
       * Period after which a non-validated identity is deleted
       **/
      validationPeriod: BlockNumber & AugmentedConst<ApiType>;
    };
    system: {
      /**
       * Maximum number of block number to block hash mappings to keep (oldest pruned first).
       **/
      blockHashCount: BlockNumber & AugmentedConst<ApiType>;
      /**
       * The maximum length of a block (in bytes).
       **/
      blockLength: BlockLength & AugmentedConst<ApiType>;
      /**
       * Block & extrinsics weights: base values and limits.
       **/
      blockWeights: BlockWeights & AugmentedConst<ApiType>;
      /**
       * The weight of runtime database operations the runtime can invoke.
       **/
      dbWeight: RuntimeDbWeight & AugmentedConst<ApiType>;
      /**
       * The designated SS85 prefix of this chain.
       * 
       * This replaces the "ss58Format" property declared in the chain spec. Reason is
       * that the runtime should know about the prefix in order to make use of it as
       * an identifier of the chain.
       **/
      ss58Prefix: u16 & AugmentedConst<ApiType>;
      /**
       * Get the chain's current version.
       **/
      version: RuntimeVersion & AugmentedConst<ApiType>;
    };
    timestamp: {
      /**
       * The minimum period between blocks. Beware that this is different to the *expected* period
       * that the block production apparatus provides. Your chosen consensus system will generally
       * work with this to determine a sensible block time. e.g. For Aura, it will be double this
       * period on default settings.
       **/
      minimumPeriod: Moment & AugmentedConst<ApiType>;
    };
    transactionPayment: {
      /**
       * The fee to be paid for making a transaction; the per-byte portion.
       **/
      transactionByteFee: BalanceOf & AugmentedConst<ApiType>;
      /**
       * The polynomial that is applied in order to derive fee from weight.
       **/
      weightToFee: Vec<WeightToFeeCoefficient> & AugmentedConst<ApiType>;
    };
    universalDividend: {
      /**
       * Square of the money growth rate per ud reevaluation period
       **/
      squareMoneyGrowthRate: Permill & AugmentedConst<ApiType>;
      /**
       * Universal dividend creation period
       **/
      udCreationPeriod: BlockNumber & AugmentedConst<ApiType>;
      /**
       * Universal dividend reevaluation period (in number of creation period)
       **/
      udReevalPeriod: BalanceOf & AugmentedConst<ApiType>;
      /**
       * Universal dividend reevaluation period in number of blocks
       * Must be equal to UdReevalPeriod * UdCreationPeriod
       **/
      udReevalPeriodInBlocks: BlockNumber & AugmentedConst<ApiType>;
    };
  }

  export interface QueryableConsts<ApiType extends ApiTypes> extends AugmentedConsts<ApiType> {
  }
}
