// Auto-generated via `yarn polkadot-types-from-chain`, do not edit
/* eslint-disable */

import type { Option, bool, u32, u8 } from '@polkadot/types';
import type { BalanceStatus } from '@polkadot/types/interfaces/balances';
import type { AuthorityList } from '@polkadot/types/interfaces/grandpa';
import type { AccountId, Balance, BalanceOf, Hash } from '@polkadot/types/interfaces/runtime';
import type { DispatchError, DispatchInfo, DispatchResult } from '@polkadot/types/interfaces/system';
import type { IdtyDid, IdtyIndex, IdtyRight } from '@duniter/core-types/interfaces/runtime';
import type { ApiTypes } from '@polkadot/api/types';

declare module '@polkadot/api/types/events' {
  export interface AugmentedEvents<ApiType> {
    balances: {
      /**
       * A balance was set by root. \[who, free, reserved\]
       **/
      BalanceSet: AugmentedEvent<ApiType, [AccountId, Balance, Balance]>;
      /**
       * Some amount was deposited (e.g. for transaction fees). \[who, deposit\]
       **/
      Deposit: AugmentedEvent<ApiType, [AccountId, Balance]>;
      /**
       * An account was removed whose balance was non-zero but below ExistentialDeposit,
       * resulting in an outright loss. \[account, balance\]
       **/
      DustLost: AugmentedEvent<ApiType, [AccountId, Balance]>;
      /**
       * An account was created with some free balance. \[account, free_balance\]
       **/
      Endowed: AugmentedEvent<ApiType, [AccountId, Balance]>;
      /**
       * Some balance was reserved (moved from free to reserved). \[who, value\]
       **/
      Reserved: AugmentedEvent<ApiType, [AccountId, Balance]>;
      /**
       * Some balance was moved from the reserve of the first account to the second account.
       * Final argument indicates the destination balance type.
       * \[from, to, balance, destination_status\]
       **/
      ReserveRepatriated: AugmentedEvent<ApiType, [AccountId, AccountId, Balance, BalanceStatus]>;
      /**
       * Transfer succeeded. \[from, to, value\]
       **/
      Transfer: AugmentedEvent<ApiType, [AccountId, AccountId, Balance]>;
      /**
       * Some balance was unreserved (moved from reserved to free). \[who, value\]
       **/
      Unreserved: AugmentedEvent<ApiType, [AccountId, Balance]>;
    };
    grandpa: {
      /**
       * New authority set has been applied. \[authority_set\]
       **/
      NewAuthorities: AugmentedEvent<ApiType, [AuthorityList]>;
      /**
       * Current authority set has been paused.
       **/
      Paused: AugmentedEvent<ApiType, []>;
      /**
       * Current authority set has been resumed.
       **/
      Resumed: AugmentedEvent<ApiType, []>;
    };
    identity: {
      /**
       * An identity has acquired a new right
       * [idty, right]
       **/
      IdtyAcquireRight: AugmentedEvent<ApiType, [IdtyDid, IdtyRight]>;
      /**
       * An identity has been confirmed by it's owner
       * [idty]
       **/
      IdtyConfirmed: AugmentedEvent<ApiType, [IdtyDid]>;
      /**
       * A new identity has been created
       * [idty, owner_key]
       **/
      IdtyCreated: AugmentedEvent<ApiType, [IdtyDid, AccountId]>;
      /**
       * An identity lost a right
       * [idty, righ]
       **/
      IdtyLostRight: AugmentedEvent<ApiType, [IdtyDid, IdtyRight]>;
      /**
       * An identity was renewed by it's owner
       * [idty]
       **/
      IdtyRenewed: AugmentedEvent<ApiType, [IdtyDid]>;
      /**
       * An identity has modified a subkey associated with a right
       * [idty_did, right, old_subkey_opt, new_subkey_opt]
       **/
      IdtySetRightSubKey: AugmentedEvent<ApiType, [IdtyDid, IdtyRight, Option<AccountId>, Option<AccountId>]>;
      /**
       * An identity has been validated
       * [idty]
       **/
      IdtyValidated: AugmentedEvent<ApiType, [IdtyDid]>;
    };
    strongCert: {
      /**
       * New certification
       * \[issuer, issuer_issued_count, receiver, receiver_received_count\]
       **/
      NewCert: AugmentedEvent<ApiType, [IdtyIndex, u8, IdtyIndex, u32]>;
      /**
       * Removed certification
       * \[issuer, issuer_issued_count, receiver, receiver_received_count, expiration\]
       **/
      RemovedCert: AugmentedEvent<ApiType, [IdtyIndex, u8, IdtyIndex, u32, bool]>;
      /**
       * Renewed certification
       * \[issuer, receiver\]
       **/
      RenewedCert: AugmentedEvent<ApiType, [IdtyIndex, IdtyIndex]>;
    };
    sudo: {
      /**
       * The \[sudoer\] just switched identity; the old key is supplied.
       **/
      KeyChanged: AugmentedEvent<ApiType, [AccountId]>;
      /**
       * A sudo just took place. \[result\]
       **/
      Sudid: AugmentedEvent<ApiType, [DispatchResult]>;
      /**
       * A sudo just took place. \[result\]
       **/
      SudoAsDone: AugmentedEvent<ApiType, [DispatchResult]>;
    };
    system: {
      /**
       * `:code` was updated.
       **/
      CodeUpdated: AugmentedEvent<ApiType, []>;
      /**
       * An extrinsic failed. \[error, info\]
       **/
      ExtrinsicFailed: AugmentedEvent<ApiType, [DispatchError, DispatchInfo]>;
      /**
       * An extrinsic completed successfully. \[info\]
       **/
      ExtrinsicSuccess: AugmentedEvent<ApiType, [DispatchInfo]>;
      /**
       * An \[account\] was reaped.
       **/
      KilledAccount: AugmentedEvent<ApiType, [AccountId]>;
      /**
       * A new \[account\] was created.
       **/
      NewAccount: AugmentedEvent<ApiType, [AccountId]>;
      /**
       * On on-chain remark happened. \[origin, remark_hash\]
       **/
      Remarked: AugmentedEvent<ApiType, [AccountId, Hash]>;
    };
    universalDividend: {
      /**
       * A new universal dividend is created
       * [ud_amout, members_count]
       **/
      NewUdCreated: AugmentedEvent<ApiType, [BalanceOf, BalanceOf]>;
      /**
       * The universal dividend has been re-evaluated
       * [new_ud_amount, monetary_mass, members_count]
       **/
      UdReevalued: AugmentedEvent<ApiType, [BalanceOf, BalanceOf, BalanceOf]>;
    };
  }

  export interface DecoratedEvents<ApiType extends ApiTypes> extends AugmentedEvents<ApiType> {
  }
}
