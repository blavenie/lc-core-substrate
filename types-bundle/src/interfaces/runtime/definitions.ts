/* eslint-disable @typescript-eslint/camelcase */

export default {
    types: {
        Balance: "u64",
        CertValue: {
            chainable_on: "BlockNumber",
            removable_on: "BlockNumber"
        },
        IdtyCertMeta: {
            issued_count: "u8",
            next_issuable_on: "BlockNumber",
            received_count: "u32"
        },
        IdtyData: {
            can_create_on: "BlockNumber"
        },
        IdtyDid: {
            hash: "[u8; 32]",
            planet: "Planet",
            latitude: "u32",
            longitude: "u32"
        },
        IdtyIndex: "u64",
        IdtyRight: {
            _enum: [
                "CreateIdty",
                "LightCert",
                "StrongCert",
                "Ud"
            ]
        },
        IdtyStatus: {
            _enum: [
                "Created",
                "ConfirmedByOwner",
                "Validated"
            ]
        },
        IdtyValue: {
            did: "IdtyDid",
            expire_on: "BlockNumber",
            owner_key: "AccountId",
            removable_on: "BlockNumber",
            renewable_on: "BlockNumber",
            rights: "Vec<(T::IdtyRight, Option<T::AccountId>)>",
            status: "IdtyStatus",
            data: "IdtyData"
        },
        Planet: {
            _enum: [
                "Earth"
            ]
        }
    }
};