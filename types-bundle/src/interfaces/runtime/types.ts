// Auto-generated via `yarn polkadot-types-from-defs`, do not edit
/* eslint-disable */

import type { Enum, Option, Struct, U8aFixed, Vec, u32, u64, u8 } from '@polkadot/types';
import type { ITuple } from '@polkadot/types/types';
import type { AccountId, BlockNumber } from '@polkadot/types/interfaces';

/** @name Balance */
export interface Balance extends u64 {}

/** @name CertValue */
export interface CertValue extends Struct {
  readonly chainable_on: BlockNumber;
  readonly removable_on: BlockNumber;
}

/** @name IdtyCertMeta */
export interface IdtyCertMeta extends Struct {
  readonly issued_count: u8;
  readonly next_issuable_on: BlockNumber;
  readonly received_count: u32;
}

/** @name IdtyData */
export interface IdtyData extends Struct {
  readonly can_create_on: BlockNumber;
}

/** @name IdtyDid */
export interface IdtyDid extends Struct {
  readonly hash: U8aFixed;
  readonly planet: Planet;
  readonly latitude: u32;
  readonly longitude: u32;
}

/** @name IdtyIndex */
export interface IdtyIndex extends u64 {}

/** @name IdtyRight */
export interface IdtyRight extends Enum {
  readonly isCreateIdty: boolean;
  readonly isLightCert: boolean;
  readonly isStrongCert: boolean;
  readonly isUd: boolean;
}

/** @name IdtyStatus */
export interface IdtyStatus extends Enum {
  readonly isCreated: boolean;
  readonly isConfirmedByOwner: boolean;
  readonly isValidated: boolean;
}

/** @name IdtyValue */
export interface IdtyValue extends Struct {
  readonly did: IdtyDid;
  readonly expire_on: BlockNumber;
  readonly owner_key: AccountId;
  readonly removable_on: BlockNumber;
  readonly renewable_on: BlockNumber;
  readonly rights: Vec<ITuple<[IdtyRight, Option<AccountId>]>>;
  readonly status: IdtyStatus;
  readonly data: IdtyData;
}

/** @name Planet */
export interface Planet extends Enum {
  readonly isEarth: boolean;
}

export type PHANTOM_RUNTIME = 'runtime';
