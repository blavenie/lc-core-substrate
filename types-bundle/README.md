# lc-core types

This npm package contains lc-core types generated with: https://polkadot.js.org/docs/api/examples/promise/typegen/

## Update types

1. Update manually file `src/interfaces/runtime/definitions.ts`
2. yarn
3. yarn build
4. yarn lint
5. fix eventually errors
